var exec = Npm.require('child_process').exec;
var events = Npm.require("events");
var eventEmitter = new events.EventEmitter();

eventEmitter.on('converted', function() {
  console.log('successfully converted');
});

eventEmitter.on('completed', function() {
  console.log('successfully completed');
})

if (pdf2htmlEX === undefined)
  pdf2htmlEX = {};

/**
 * Execute pdf2htmlEX in command line
 * @param  {array}    args: pdf2htmlEX args
 * @param  {Function} callback: the callback
 */
pdf2htmlEX.execute = function (args, callback) {
  var command = 'pdf2htmlEX ' + args.join(' ');
   
  exec(command, function(err, stdout, stderr) {
    if(err){ 
      callback(new Error(err));
    }
    else {
      callback(null, new Buffer(stdout));//unneccesary
    }
  });

  eventEmitter.emit('converted');
};

/**
 * Methods output resulting html file in current directory
 */

pdf2htmlEX.zoom = function (pdf, scale, callback) {
  pdf2htmlEX.execute(['--zoom', scale, pdf], callback);
  eventEmitter.emit('completed');
};

pdf2htmlEX.split = function(pdf, callback) {
  pdf2htmlEX.execute(['--split-pages', 1, pdf], callback);
};

pdf2htmlEX.pageFilename = function(pdf, filename, callback) {
  pdf2htmlEX.execute(['--split-pages', 1, '--page-filename', filename, pdf], callback);
};

pdf2htmlEX.embed = function(pdf, switches, callback) {
  pdf2htmlEX.execute(['--embed', switches, pdf], callback);
};

pdf2htmlEX.custom = function(pdf, args, callback) {
  pdf2htmlEX.execute([args, pdf], callback);
};
